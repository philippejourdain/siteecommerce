<?php
return array(
  'mini-module\form\new_user' => array(
      'elements' => array(
        array(
            'spec' => array(
                'type' => '\Autentification\Form\Element\Login',
            )
        ),
          array(
              'spec' => array(
                  'type' => '\Zend\Form\Element\Captcha',
                  'name' => 'newUserCaptcha',
                  'options' => array(
                      'label' => 'Vérification que vous n\'etes pas une machine',
                      'captcha' => array(
                          'class' => 'Dumb',
                      ),
                  ),
              ),
          ),
      ),
  )
);